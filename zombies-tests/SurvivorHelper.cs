﻿using zombies.Models;
using zombies.Models.Factories;

namespace zombies_tests
{
  public static class SurvivorHelper
  {
    public static SurvivorFactory SurvivorFactory = new SurvivorFactory();
    public static string[] SurvivorNames = new string[] { "Jack", "Sarah", "Michael" };
    public static Survivor GetSurvivor()
    {
      return SurvivorFactory.Create(SurvivorNames[0]);
    }
  }
}
