﻿using Xunit;
using zombies.Models;
using zombies.Models.Exceptions;

namespace zombies_tests
{
  public class ExceptionTests
  {
    [Fact]
    public void RepeatedSurvivorNameException_with_object_shows_correct_message()
    {
      var survivor = SurvivorHelper.GetSurvivor();
      var expected = $"There already is a survivor with the name of {survivor.Name}.";
      var actual = new RepeatedSurvivorNameException(survivor);

      Assert.Equal(expected, actual.Message);
    }

    [Fact]
    public void RepeatedSurvivorNameException_with_null_object_shows_correct_message()
    {
      var expected = $"There already is a survivor with that name.";
      var actual = new RepeatedSurvivorNameException((Survivor)null);

      Assert.Equal(expected, actual.Message);
    }

    [Fact]
    public void RepeatedSurvivorNameException_with_string_shows_correct_message()
    {
      var survivorName = SurvivorHelper.SurvivorNames[0];
      var expected = $"There already is a survivor with the name of {survivorName}.";
      var actual = new RepeatedSurvivorNameException(survivorName);

      Assert.Equal(expected, actual.Message);
    }

  }
}
