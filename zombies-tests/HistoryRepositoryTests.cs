﻿using System;
using System.Linq;
using Xunit;
using zombies.Models;
using zombies.Repositories;

namespace zombies_tests
{
  public class HistoryRepositoryTests
  {
    private readonly DateTime _timeStamp1;
    private readonly DateTime _timeStamp2;
    private readonly DateTime _timeStamp3;

    public HistoryRepositoryTests()
    {
      _timeStamp1 = new DateTime(2020, 1, 1);
      _timeStamp2 = new DateTime(2020, 3, 1);
      _timeStamp3 = new DateTime(2020, 2, 1);
    }

    [Fact]
    public void AddEvent_inserts_event_to_history()
    {
      var historyRepository = new HistoryRepository();

      Assert.Empty(historyRepository.GetHistory);

      historyRepository.AddEvent(HistoryEventType.GameStarted, _timeStamp1);

      Assert.Single(historyRepository.GetHistory);

      historyRepository.AddEvent(HistoryEventType.GameEnded, _timeStamp2);
      historyRepository.AddEvent(HistoryEventType.SurvivorAdded, _timeStamp3);

      Assert.Equal(3, historyRepository.GetHistory.Count);

      var event1 = historyRepository.GetHistory.FirstOrDefault(h => h.EventType == HistoryEventType.GameStarted && h.TimeStamp == _timeStamp1);
      var event2 = historyRepository.GetHistory.FirstOrDefault(h => h.EventType == HistoryEventType.GameEnded && h.TimeStamp == _timeStamp2);
      var event3 = historyRepository.GetHistory.FirstOrDefault(h => h.EventType == HistoryEventType.SurvivorAdded && h.TimeStamp == _timeStamp3);

      Assert.NotNull(event1);
      Assert.NotEqual(new Guid(), event1.Id);
      Assert.NotNull(event2);
      Assert.NotNull(event3);
    }

    [Fact]
    public void RemoveEvent_deletes_event_from_history()
    {
      var historyRepository = new HistoryRepository();
      historyRepository.AddEvent(HistoryEventType.GameStarted, _timeStamp1);
      historyRepository.AddEvent(HistoryEventType.GameEnded, _timeStamp2);
      historyRepository.AddEvent(HistoryEventType.SurvivorAdded, _timeStamp3);

      Assert.Equal(3, historyRepository.GetHistory.Count);

      historyRepository.RemoveLastEvent();

      Assert.Equal(2, historyRepository.GetHistory.Count);

      var event1 = historyRepository.GetHistory.FirstOrDefault(h => h.EventType == HistoryEventType.GameStarted && h.TimeStamp == _timeStamp1);
      var event2 = historyRepository.GetHistory.FirstOrDefault(h => h.EventType == HistoryEventType.SurvivorAdded && h.TimeStamp == _timeStamp3);

      Assert.NotNull(event1);
      Assert.NotNull(event2);

      historyRepository.RemoveLastEvent();

      Assert.Single(historyRepository.GetHistory);
      Assert.NotNull(historyRepository.GetHistory.FirstOrDefault(h => h.EventType == HistoryEventType.GameStarted && h.TimeStamp == _timeStamp1));
    }
  }
}
