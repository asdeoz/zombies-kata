﻿using System;
using System.Linq;
using Xunit;
using zombies.Commands;
using zombies.Models;
using zombies.Repositories;

namespace zombies_tests
{
  public class CommandManagerTests
  {
    private readonly DateTime _timeStamp1;
    private readonly DateTime _timeStamp2;

    public CommandManagerTests()
    {
      _timeStamp1 = new DateTime(2020, 1, 1);
      _timeStamp2 = new DateTime(2020, 2, 1);
    }

    [Fact]
    public void Invoke_adds_event_to_history()
    {
      var manager = new CommandManager();
      var repo = new HistoryRepository();

      manager.Invoke(new AddToHistoryCommand(repo, HistoryEventType.GameStarted, _timeStamp1));

      Assert.Single(repo.GetHistory);

      manager.Invoke(new AddToHistoryCommand(repo, HistoryEventType.GameEnded, _timeStamp2));

      Assert.Equal(2, repo.GetHistory.Count);
    }

    [Fact]
    public void Undo_removes_from_history_and_adds_tempered()
    {
      var manager = new CommandManager();
      var repo = new HistoryRepository();

      manager.Invoke(new AddToHistoryCommand(repo, HistoryEventType.GameStarted, _timeStamp1));
      manager.Invoke(new AddToHistoryCommand(repo, HistoryEventType.GameEnded, _timeStamp2));

      Assert.Equal(2, repo.GetHistory.Count);

      manager.Undo();

      Assert.Equal(2, repo.GetHistory.Count);

      var event1 = repo.GetHistory.FirstOrDefault(h => h.EventType == HistoryEventType.GameStarted);
      var event2 = repo.GetHistory.FirstOrDefault(h => h.EventType == HistoryEventType.GameEnded);
      var temperEvent = repo.GetHistory.FirstOrDefault(h => h.EventType == HistoryEventType.HistoryTempered);

      Assert.NotNull(event1);
      Assert.NotNull(temperEvent);
      Assert.Null(event2);
    }
  }
}
