﻿using System;
using Xunit;
using zombies.Models;

namespace zombies_tests
{
  public class SurvivorTest
  {
    public SurvivorTest()
    {
    }

    [Fact]
    public void Survivor_contructor_builds_starting_survivor()
    {
      var survivor = SurvivorHelper.GetSurvivor();

      Assert.Equal(SurvivorHelper.SurvivorNames[0], survivor.Name);
      Assert.Equal(ColorLevel.Blue, survivor.Level);
      Assert.Equal(0, survivor.Wounds);
      Assert.Equal(3, survivor.Actions);
      Assert.Equal(0, survivor.Experience);
      Assert.NotNull(survivor.Backpack);
      Assert.Empty(survivor.Backpack);
      Assert.Null(survivor.Equipment1);
      Assert.Null(survivor.Equipment2);
    }

    [Fact]
    public void Survivor_levels_up()
    {
      var survivor = SurvivorHelper.GetSurvivor();

      Assert.Equal(0, survivor.Experience);
      Assert.Equal(ColorLevel.Blue, survivor.Level);
      survivor.AddExperience();
      Assert.Equal(1, survivor.Experience);
      Assert.Equal(ColorLevel.Blue, survivor.Level);
      survivor.AddExperience(5);
      Assert.Equal(6, survivor.Experience);
      Assert.Equal(ColorLevel.Blue, survivor.Level);
      survivor.AddExperience();
      Assert.Equal(7, survivor.Experience);
      Assert.Equal(ColorLevel.Yellow, survivor.Level);
      survivor.AddExperience(12);
      Assert.Equal(19, survivor.Experience);
      Assert.Equal(ColorLevel.Orange, survivor.Level);
      survivor.AddExperience(24);
      Assert.Equal(43, survivor.Experience);
      Assert.Equal(ColorLevel.Red, survivor.Level);
    }

    [Fact]
    public void Survivor_gets_wounded()
    {
      var survivor = SurvivorHelper.GetSurvivor();

      Assert.Equal(3, survivor.Actions);
      survivor.SpendAction();
      Assert.Equal(2, survivor.Actions);
      survivor.SpendAction();
      survivor.SpendAction();
      Assert.Equal(0, survivor.Actions);
      survivor.SpendAction();
      Assert.Equal(0, survivor.Actions);
      survivor.ResetActions();
      Assert.Equal(3, survivor.Actions);
    }

    [Fact]
    public void SurvivorCreated_is_raised()
    {
      Survivor actual = null;

      SurvivorHelper.SurvivorFactory.SurvivorCreated += delegate (object sender, EventArgs e)
      {
        actual = (Survivor)sender;
      };

      var expected = SurvivorHelper.GetSurvivor();

      Assert.NotNull(actual);
      Assert.Equal(expected, actual);
    }
  }
}
