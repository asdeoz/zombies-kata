﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using zombies.Models;

namespace zombies.Repositories
{
  public class HistoryRepository : IHistoryRepository
  {
    private readonly List<HistoryEvent> _history;

    public HistoryRepository()
    {
      _history = new List<HistoryEvent>();
    }

    public ReadOnlyCollection<HistoryEvent> GetHistory => _history.AsReadOnly();

    public void AddEvent(HistoryEventType historyEventType, DateTime timeStamp)
    {
      _history.Add(new HistoryEvent
      {
        Id = Guid.NewGuid(),
        EventType = historyEventType,
        TimeStamp = timeStamp,
      });
    }

    public void RemoveLastEvent()
    {
      if (_history.Count == 0) return;
      var first = _history.OrderByDescending(e => e.TimeStamp).First();
      _history.Remove(first);
    }
  }
}
