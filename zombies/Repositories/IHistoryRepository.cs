﻿using System;
using System.Collections.ObjectModel;
using zombies.Models;

namespace zombies.Repositories
{
  public interface IHistoryRepository
  {
    void AddEvent(HistoryEventType historyEventType, DateTime timeStamp);
    void RemoveLastEvent();
    ReadOnlyCollection<HistoryEvent> GetHistory { get; }
  }
}
