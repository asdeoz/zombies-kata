﻿using System;
using zombies.Models;
using zombies.Repositories;

namespace zombies.Commands
{
  public class AddToHistoryCommand : ICommand
  {
    private readonly IHistoryRepository _historyRepository;
    private readonly HistoryEventType _historyEventType;
    private readonly DateTime _timeStamp;

    public AddToHistoryCommand(IHistoryRepository historyRepository, HistoryEventType historyEventType, DateTime? timeStamp)
    {
      _historyRepository = historyRepository;
      _historyEventType = historyEventType;
      _timeStamp = timeStamp ?? DateTime.Now;
    }
    public bool CanExecute()
    {
      return _historyRepository != null;
    }

    public void Execute()
    {
      _historyRepository.AddEvent(_historyEventType, _timeStamp);
    }

    public void Undo()
    {
      _historyRepository.RemoveLastEvent();
      _historyRepository.AddEvent(HistoryEventType.HistoryTempered, DateTime.Now);
    }
  }
}
