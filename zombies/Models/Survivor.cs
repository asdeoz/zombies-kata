﻿using System;
using System.Collections.Generic;
using System.Linq;
using zombies.Models.Exceptions;

namespace zombies.Models
{
  public class Survivor
  {
    private const int STARTING_EQUIPMENT = 5;
    private const int MAX_WOUNDS = 2;
    private const int STARTING_ACTIONS = 3;

    public string Name { get; set; }
    public int Wounds { get; private set; }
    public int Actions { get; set; }
    public Equipment Equipment1 { get; private set; }
    public Equipment Equipment2 { get; private set; }
    public List<Equipment> Backpack { get; set; }
    public int Experience { get; private set; }
    public ColorLevel Level { get; private set; }
    public SkillTree SkillTree { get; set; }
    public bool IsDead { 
      get {
        return Wounds >= MAX_WOUNDS;
      }
    }

    public event EventHandler<EventArgs> SurvivorWounded;
    public event EventHandler<EventArgs> SurvivorDied;
    public event EventHandler<EventArgs> SurvivorAcquiredGear;
    public event EventHandler<EventArgs> SurvivorLeveledUp;
    public event EventHandler<EventArgs> SurvivorGainedSkill;

    public Survivor(string name)
    {
      Name = name;
      Wounds = 0;
      Actions = STARTING_ACTIONS;
      Backpack = new List<Equipment>();
      Experience = 0;
      SkillTree = new SkillTree();
    }

    public void ResetActions()
    {
      Actions = GetMaxActions();
    }

    public void SpendAction()
    {
      Actions = Math.Max(0, Actions - 1);
    }

    public void Wound()
    {
      Wounds += Wounds < MAX_WOUNDS ? 1 : 0;
      SurvivorWounded(this, new EventArgs());
      if (Wounds >= MAX_WOUNDS) SurvivorDied(this, new EventArgs());
    }

    public void Equip(string EquipmentName)
    {
      var item = Backpack.FirstOrDefault(e => e.Name == EquipmentName);
      if (item == null) throw new NotInBackpackException(EquipmentName);
      if (Equipment1 == null)
      {
        Equipment1 = item;
        return;
      }
      if(Equipment2 == null)
      {
        Equipment2 = item;
        return;
      }
      throw new BackpackFullException();
    }

    public void Carry(Equipment equipment)
    {
      if (Backpack.Count <= GetMaxEquipment())
      {
        Backpack.Add(equipment);
        SurvivorAcquiredGear(this, new EventArgs());
      }
    }

    public void AddExperience(int exp = 1)
    {
      Experience += exp;
      CheckLevelChange();
    }

    private void CheckLevelChange()
    {
      var currentLevel = Level;

      if (Experience > 129 && !SkillTree.Skill6.HasValue)
      {
        SurvivorGainedSkill(this, new EventArgs());
      }
      if (Experience > 86 && !SkillTree.Skill5.HasValue)
      {
        SurvivorGainedSkill(this, new EventArgs());
      }
      if (Experience > 61 && !SkillTree.Skill4.HasValue)
      {
        SurvivorGainedSkill(this, new EventArgs());
      }
      if (Experience > 42 && Level == ColorLevel.Orange)
      {
        Level = ColorLevel.Red;
        SurvivorGainedSkill(this, new EventArgs());
      }
      if (Experience > 18 && Level == ColorLevel.Yellow)
      {
        Level = ColorLevel.Red;
        SurvivorGainedSkill(this, new EventArgs());
      }
      if (Experience > 6 && Level == ColorLevel.Blue)
      {
        Level = ColorLevel.Yellow;
        SkillTree.Skill1 = Skill.ExtraAction;
      }

      if (currentLevel != Level) SurvivorLeveledUp(this, new EventArgs());
    }

    private int GetMaxActions()
    {
      return STARTING_ACTIONS + SkillTree.Skill1 == Skill.ExtraAction ? 1 : 0;
    }

    private int GetMaxEquipment()
    {
      var hasHoardSkill = SkillTree.Skill2 == Skill.Hoard || SkillTree.Skill3 == Skill.Hoard || SkillTree.Skill4 == Skill.Hoard || SkillTree.Skill5 == Skill.Hoard || SkillTree.Skill6 == Skill.Hoard;
      return STARTING_EQUIPMENT - Wounds + (hasHoardSkill ? 1 : 0);
    }
  }
}
