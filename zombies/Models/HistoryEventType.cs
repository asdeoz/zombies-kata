﻿namespace zombies.Models
{
  public enum HistoryEventType
  {
    GameStarted,
    SurvivorAdded,
    SurvivorGotEquipment,
    SurvivorGotWounded,
    SurvivorDied,
    SurvivorLevelsUp,
    SurvivorHasNewSkill,
    GameLevelChanged,
    GameEnded,
    HistoryTempered,
  }
}
