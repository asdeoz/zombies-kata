﻿namespace zombies.Models
{
  public class SkillTree
  {
    public Skill? Skill1 { get; set; }
    public Skill? Skill2 { get; set; }
    public Skill? Skill3 { get; set; }
    public Skill? Skill4 { get; set; }
    public Skill? Skill5 { get; set; }
    public Skill? Skill6 { get; set; }
  }
}
