﻿using System;

namespace zombies.Models.Exceptions
{
  public class GameOverException : Exception
  {
    public override string Message => "All survivors died. Game over.";
  }
}
