﻿using System;

namespace zombies.Models.Exceptions
{
  public class NotInBackpackException : Exception
  {
    private readonly string _itemName;
    public NotInBackpackException(string itemName)
    {
      _itemName = itemName;
    }
    public override string Message => $"Equipment piece \"{_itemName}\" is not being carried.";
  }
}
