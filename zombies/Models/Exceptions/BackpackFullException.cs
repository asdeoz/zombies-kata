﻿using System;

namespace zombies.Models.Exceptions
{
  public class BackpackFullException : Exception
  {
    public override string Message => "All equipment slots are full.";
  }
}
