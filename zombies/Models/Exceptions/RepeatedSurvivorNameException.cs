﻿using System;

namespace zombies.Models.Exceptions
{
  public class RepeatedSurvivorNameException : Exception
  {
    private readonly string _survivorName;

    public RepeatedSurvivorNameException(string survivorName)
    {
      _survivorName = survivorName;
    }

    public RepeatedSurvivorNameException(Survivor survivor)
    {
      _survivorName = survivor?.Name ?? string.Empty;
    }

    public override string Message => 
      string.IsNullOrWhiteSpace(_survivorName) ? "There already is a survivor with that name." : $"There already is a survivor with the name of {_survivorName}.";
  }
}
