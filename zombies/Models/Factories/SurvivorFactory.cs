﻿using System;

namespace zombies.Models.Factories
{
  public class SurvivorFactory
  {
    public event EventHandler<EventArgs> SurvivorCreated;
    public Survivor Create(string survivorName)
    {
      var result = new Survivor(survivorName);
      SurvivorCreated?.Invoke(result, new EventArgs());
      return result;
    }
  }
}
