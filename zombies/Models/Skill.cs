﻿namespace zombies.Models
{
  public enum Skill
  {
    ExtraAction,
    ExtraMeleeDie,
    ExtraRangedDie,
    ExtraFreeMove,
    Hoard,
    Sniper,
    Tough,
  }
}
