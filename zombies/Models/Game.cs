﻿using System;
using System.Collections.Generic;
using System.Linq;
using zombies.Commands;
using zombies.Models.Exceptions;
using zombies.Models.Factories;
using zombies.Repositories;

namespace zombies.Models
{
  public class Game
  {
    public Dictionary<string, Survivor> Survivors { get; private set; }
    public ColorLevel Level { get; private set; }
    private readonly IHistoryRepository _historyRepository;
    private readonly CommandManager _manager;
    private readonly SurvivorFactory _survivorFactory;

    public Game()
    {
      Survivors = new Dictionary<string, Survivor>();
      Level = ColorLevel.Blue;
      _historyRepository = new HistoryRepository();
      _survivorFactory = new SurvivorFactory();
      _manager = new CommandManager();
      _manager.Invoke(new AddToHistoryCommand(_historyRepository, HistoryEventType.GameStarted, DateTime.Now));
      _survivorFactory.SurvivorCreated += Survivor_SurvivorCreated;
    }

    public void AddSurvivor(string survivorName)
    {
      var survivor = _survivorFactory.Create(survivorName);

      if (Survivors.ContainsKey(survivor.Name))
      {
        throw new RepeatedSurvivorNameException(survivor);
      }

      Survivors.Add(survivor.Name, survivor);

      survivor.SurvivorDied += Survivor_SurvivorDied;
      survivor.SurvivorWounded += Survivor_SurvivorWounded;
      survivor.SurvivorLeveledUp += Survivor_SurvivorLeveledUp;
      survivor.SurvivorAcquiredGear += Survivor_SurvivorAcquiredGear;
      survivor.SurvivorGainedSkill += Survivor_SurvivorGainedSkill;
    }

    public void RemoveSurvivor(string survivorName)
    {
      Survivors.Remove(survivorName);

      if (Survivors.Count <= 0)
      {
        _manager.Invoke(new AddToHistoryCommand(_historyRepository, HistoryEventType.GameEnded, DateTime.Now));
      }
    }

    private ColorLevel GetHighestSurivorLevel()
    {
      var survivorsAlive = Survivors.Where(s => !s.Value.IsDead);
      if (survivorsAlive == null || survivorsAlive.Count() == 0) return ColorLevel.Blue;
      var survivor = survivorsAlive.OrderByDescending(s => s.Value.Experience).First();
      return survivor.Value.Level;
    }

    private void CheckForGameLevelChange()
    {
      var currentLevel = GetHighestSurivorLevel();
      if (currentLevel != Level)
      {
        Level = currentLevel;
        _manager.Invoke(new AddToHistoryCommand(_historyRepository, HistoryEventType.GameLevelChanged, DateTime.Now));
      }
    }

    #region Survivor Events Subscription

    private void Survivor_SurvivorCreated(object sender, EventArgs e)
    {
      _manager.Invoke(new AddToHistoryCommand(_historyRepository, HistoryEventType.SurvivorAdded, DateTime.Now));
    }

    private void Survivor_SurvivorDied(object sender, EventArgs e)
    {
      _manager.Invoke(new AddToHistoryCommand(_historyRepository, HistoryEventType.SurvivorDied, DateTime.Now));
      CheckForGameLevelChange();
    }
    private void Survivor_SurvivorAcquiredGear(object sender, EventArgs e)
    {
      _manager.Invoke(new AddToHistoryCommand(_historyRepository, HistoryEventType.SurvivorGotEquipment, DateTime.Now));
    }

    private void Survivor_SurvivorLeveledUp(object sender, EventArgs e)
    {
      _manager.Invoke(new AddToHistoryCommand(_historyRepository, HistoryEventType.SurvivorLevelsUp, DateTime.Now));
      CheckForGameLevelChange();
    }

    private void Survivor_SurvivorWounded(object sender, EventArgs e)
    {
      _manager.Invoke(new AddToHistoryCommand(_historyRepository, HistoryEventType.SurvivorGotWounded, DateTime.Now));
    }

    private void Survivor_SurvivorGainedSkill(object sender, EventArgs e)
    {
      var survivor = (Survivor)sender;
      Console.WriteLine($"Time to get a new skill for {survivor.Name}");
      _manager.Invoke(new AddToHistoryCommand(_historyRepository, HistoryEventType.SurvivorHasNewSkill, DateTime.Now));
    }

    #endregion

  }
}
