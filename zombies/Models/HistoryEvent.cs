﻿using System;

namespace zombies.Models
{
  public class HistoryEvent
  {
    public Guid Id { get; set; }
    public HistoryEventType EventType { get; set; }
    public DateTime TimeStamp { get; set; }
  }
}
